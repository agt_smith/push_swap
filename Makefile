# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/08 16:39:52 by gsmith            #+#    #+#              #
#    Updated: 2018/01/09 15:16:07 by gsmith           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=$(NAME1) $(NAME2)
NAME1=checker
NAME2=push_swap
COMP=clang
FLAG=-Wall -Wextra -Werror
LIBFT=ft
LIBFT_DIR=libft/
LIBFT_HEADER=-I $(LIBFT_DIR)libft/ -I $(LIBFT_DIR)ft_printf/ \
		-I $(LIBFT_DIR)get_next_line/
HEADER_PATH=includes/
OBJS_PATH=objs/
COMMON_PATH=common/
CHECKER_PATH=checker_srcs/
PUSH_SWAP_PATH=push_swap_srcs/
COMMON_SRCS=$(addprefix $(COMMON_PATH),read_arg.c \
		print_stack.c \
		stack_op.c \
		stack_end.c)
CHECKER_SRCS=$(addprefix $(CHECKER_PATH),checker.c \
		read_op.c \
		stack_do_op.c)
PUSH_SWAP_SRCS=$(addprefix $(PUSH_SWAP_PATH),push_swap.c \
		sort_st.c \
		simple_sort.c \
		complex_sort.c \
		tab.c \
		clean_op.c \
		op_util.c \
		op_util2.c)
COMMON_OBJS=$(COMMON_SRCS:$(COMMON_PATH)%.c=$(OBJS_PATH)%.o)
CHECKER_OBJS=$(CHECKER_SRCS:$(CHECKER_PATH)%.c=$(OBJS_PATH)%.o)
PUSH_SWAP_OBJS=$(PUSH_SWAP_SRCS:$(PUSH_SWAP_PATH)%.c=$(OBJS_PATH)%.o)
ERASE_LINE='\r\033[K'
NC='\033[0m'
RED='\033[0;31m'
LRED='\033[1;31m'
LBLUE='\033[1;34m'
GREEN='\033[0;32m'
LGREEN='\033[1;32m'
ORANGE='\033[0;33m'
YELLOW='\033[1;33m'

all: $(NAME)

$(NAME1): $(COMMON_OBJS) $(CHECKER_OBJS)
	@printf $(ERASE_LINE)"[$(NAME1)]"$(YELLOW)"Compiling .o files done.\n"$(NC)
	@make -C $(LIBFT_DIR)
	@printf "[$(NAME1)]"$(LBLUE)"Compiling $(NAME1)..."$(NC)
	@$(COMP) $(FLAG) -I $(HEADER_PATH) $(LIBFT_HEADER) -o $(NAME1) \
		$(COMMON_OBJS) $(CHECKER_OBJS) \
		-L./$(LIBFT_DIR) -l$(LIBFT)
	@printf $(ERASE_LINE)"[$(NAME1)]"$(LGREEN)"$(NAME1) ready.\n"$(NC)

$(NAME2): $(COMMON_OBJS) $(PUSH_SWAP_OBJS)
	@printf $(ERASE_LINE)"[$(NAME2)]"$(YELLOW)"Compiling .o files done.\n"$(NC)
	@make -C $(LIBFT_DIR)
	@printf "[$(NAME2)]"$(LBLUE)"Compiling $(NAME2)..."$(NC)
	@$(COMP) $(FLAG) -I $(HEADER_PATH) $(LIBFT_HEADER) -o $(NAME2) \
		$(COMMON_OBJS) $(PUSH_SWAP_OBJS) \
		-L./$(LIBFT_DIR) -l$(LIBFT)
	@printf $(ERASE_LINE)"[$(NAME2)]"$(LGREEN)"$(NAME2) ready.\n"$(NC)

$(OBJS_PATH)%.o: $(COMMON_PATH)%.c
	@printf $(ERASE_LINE)"[$(NAME)]"$(LBLUE)"Compiling $<..."$(NC)
	@$(COMP) $(FLAG) -I $(HEADER_PATH) $(LIBFT_HEADER) -o $@ -c $<

$(OBJS_PATH)%.o: $(CHECKER_PATH)%.c
	@printf $(ERASE_LINE)"[$(NAME1)]"$(LBLUE)"Compiling $<..."$(NC)
	@$(COMP) $(FLAG) -I $(HEADER_PATH) $(LIBFT_HEADER) -o $@ -c $<

$(OBJS_PATH)%.o: $(PUSH_SWAP_PATH)%.c
	@printf $(ERASE_LINE)"[$(NAME2)]"$(LBLUE)"Compiling $<..."$(NC)
	@$(COMP) $(FLAG) -I $(HEADER_PATH) $(LIBFT_HEADER) -o $@ -c $<

clean:
	@make -C $(LIBFT_DIR) clean
	@printf "[$(NAME)]"$(RED)"Cleaning .o files..."$(NC)
	@rm -f $(COMMON_OBJS) $(CHECKER_OBJS) $(PUSH_SWAP_OBJS)
	@printf $(ERASE_LINE)"[$(NAME)]"$(LRED)".o files cleaned.\n"$(NC)

fclean:
	@make -C $(LIBFT_DIR) fclean
	@printf "[$(NAME)]"$(RED)"Cleaning .o files..."$(NC)
	@rm -f $(COMMON_OBJS) $(CHECKER_OBJS) ./$(PUSH_SWAP_OBJS)
	@printf $(ERASE_LINE)"[$(NAME)]"$(LRED)".o files cleaned.\n"$(NC)
	@printf "[$(NAME)]"$(RED)"Cleaning $(NAME) file..."$(NC)
	@rm -f $(NAME)
	@printf $(ERASE_LINE)"[$(NAME)]"$(LRED)"$(NAME) cleaned.\n"$(NC)

re: fclean all
