/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 16:51:39 by gsmith            #+#    #+#             */
/*   Updated: 2017/12/17 11:51:44 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TAB_H
# define TAB_H
# include "libft.h"

int				*list_to_tab(t_list *lst, int len);
void			tab_quicksort(int *tab, int len);
t_bool			is_in_tab(int *tab, int len, int value);
void			print_tab(int *tab, int len);

#endif
