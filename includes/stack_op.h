/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_op.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 19:13:39 by gsmith            #+#    #+#             */
/*   Updated: 2017/12/08 14:44:53 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_OP_H
# define STACK_OP_H
# define NB_OP 4
# include "libft.h"

typedef struct	s_select_op
{
	char	id;
	void	(*op)(t_list **, t_list **);
}				t_select_op;

typedef struct	s_do_op
{
	char	id_op;
	char	id_stack;
	t_list	**stack1;
	t_list	**stack2;
	void	(*op)(t_list **, t_list **);
}				t_do_op;

void			exec_op(t_list *op);

void			stack_swap(t_list **stack, t_list **bis);
void			stack_push(t_list **dst, t_list **src);
void			stack_rotate(t_list **stack, t_list **bis);
void			stack_revrotate(t_list **stack, t_list **bis);

#endif
