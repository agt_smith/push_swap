/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 15:29:21 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/09 13:59:02 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORT_H
# define SORT_H
# include "stacks.h"
# include "libft.h"

typedef struct		s_stacks
{
	t_list		*a;
	t_list		*b;
	t_list		*op;
	t_env		env;
}					t_stacks;

void				simple_sort(t_stacks *stacks, int len);
void				complex_sort(t_stacks *stacks, int len);
int					get_lstpos(t_list *lst, int value, int acc);
void				goto_stpos(t_stacks *st, int value, t_bool sort_a);
t_bool				clean_conflict(t_list **op);
void				print_sorted_op(t_list *op, int fd);
void				release_op_elem(void *val, size_t size);
void				release_op(t_list **lst_op);
t_bool				compare(t_stacks *stacks, int dir, t_bool comp_a);

#endif
