/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_sort.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 16:23:27 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/09 15:15:48 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OP_SORT_H
# define OP_SORT_H
# include "sort.h"

void		swap_st(t_stacks *stacks, t_bool sort_a);
void		swap_both(t_stacks *stacks);
void		push_st(t_stacks *stacks, t_bool dest_a);
void		rotate_st(t_stacks *stacks, t_bool sort_a);
void		revrotate_st(t_stacks *stacks, t_bool sort_a);

void		small_sort_rot(t_stacks *stacks, int len, t_bool sort_a, int dir);
void		small_sort(t_stacks *stacks, int len, t_bool sort_a);
void		sort_both(t_stacks *stacks, int len);

#endif
