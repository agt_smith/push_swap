/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stacks.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 14:00:07 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/10 13:19:27 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACKS_H
# define STACKS_H
# include "libft.h"

typedef struct	s_env
{
	int		flags;
	int		fd;
}				t_env;

void			print_both(t_list *a, t_list *b, t_bool start, char *op);
int				get_flags(int argc, char *argv[], t_env *env);
void			get_stack(t_list **stack, int argc, char *argv[], int start);
void			release_stack(t_list **stack);
void			release_op(t_list **lst_op);
void			print_stack(t_list *stack);
void			print_op(t_list *stack);
t_bool			stack_is_sort(t_list *stack, int limit, int dir);
int				stack_len_sort(t_list *stack, int limit, int dir);

#endif
