/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_op.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 15:52:22 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/08 17:34:51 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sort.h"

t_bool			clean_conflict(t_list **op)
{
	t_list		*tmp;
	char		*prev;
	char		*next;
	t_bool		res;

	res = FALSE;
	if (op && *op && (*op)->next)
	{
		prev = (char *)((*op)->content);
		next = (char *)((*op)->next->content);
		if (prev[0] == 'p' && next[0] == 'p' && prev[1] != next[1])
		{
			tmp = (*op)->next->next;
			ft_lstdelone(&((*op)->next), &release_op_elem);
			ft_lstdelone(op, &release_op_elem);
			*op = tmp;
			return (TRUE);
		}
		while (clean_conflict(&((*op)->next)))
			res = TRUE;
	}
	return (res);
}
