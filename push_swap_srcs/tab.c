/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 16:49:23 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/05 17:12:13 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

#include "ft_printf.h"

int				*list_to_tab(t_list *lst, int len)
{
	int		*tab;
	int		i;
	t_list	*cur;

	if (!(tab = (int *)malloc(sizeof(int) * len)))
		ft_perror("malloc error");
	i = 0;
	cur = lst;
	while (cur && i < len)
	{
		tab[i++] = *(int *)(cur->content);
		cur = cur->next;
	}
	return (tab);
}

static t_bool	tab_is_sort(int *tab, int len)
{
	if (len < 2)
		return (TRUE);
	if (tab[len - 2] > tab[len - 1])
		return (FALSE);
	return (tab_is_sort(tab, len - 1));
}

t_bool			is_in_tab(int *tab, int len, int value)
{
	if (!len)
		return (FALSE);
	if (*tab == value)
		return (TRUE);
	return (is_in_tab(tab + 1, len - 1, value));
}

void			print_tab(int *tab, int len)
{
	int		i;

	i = 0;
	while (i < len)
		ft_printf("%d;", tab[i++]);
	ft_putchar('\n');
}

void			tab_quicksort(int *tab, int len)
{
	int		i;
	int		j;
	int		tmp;
	int		pivot;

	if (len > 1 && (i = -1) == -1)
	{
		pivot = tab[len - 1];
		j = i;
		while (tab[++i] != pivot)
			if (tab[i] > pivot)
			{
				tmp = tab[i];
				j = i--;
				while (tab[++j] != pivot)
					tab[j - 1] = tab[j];
				tab[j - 1] = tab[j];
				tab[j] = tmp;
			}
		j = (j < 0 ? len : j);
		if (!tab_is_sort(tab, len))
			tab_quicksort(tab, j - 1);
		if (!tab_is_sort(tab, len))
			tab_quicksort(tab + j, len - j);
	}
}
