/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_util.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 19:42:11 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/09 13:59:16 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op_sort.h"
#include <stdlib.h>

int				get_lstpos(t_list *lst, int value, int acc)
{
	if (!lst)
		return (-1);
	if (*(int *)(lst->content) == value)
		return (acc);
	return (get_lstpos(lst->next, value, acc + 1));
}

void			goto_stpos(t_stacks *stacks, int value, t_bool sort_a)
{
	int		offset;
	t_list	*st;
	void	(*move)(t_stacks *, t_bool);
	int		len;

	st = sort_a ? stacks->a : stacks->b;
	len = ft_lstlen(st);
	offset = get_lstpos(st, value, 0);
	move = &revrotate_st;
	if (offset > len / 2)
		offset = len - offset;
	else
		move = &rotate_st;
	while (offset-- > 0)
		(*move)(stacks, sort_a);
}

void			release_op_elem(void *val, size_t size)
{
	(void)size;
	free((char *)val);
}

void			print_sorted_op(t_list *op, int fd)
{
	if (op)
	{
		ft_putstr_fd((char *)(op->content), fd);
		print_sorted_op(op->next, fd);
	}
}

void			release_op(t_list **lst_op)
{
	ft_lstdel(lst_op, &release_op_elem);
}
