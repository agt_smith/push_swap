/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simple_sort.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 14:39:17 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/08 17:41:29 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op_sort.h"
#include "sort.h"
#include "stacks.h"
#include "tab.h"
#include <stdlib.h>

#include "ft_printf.h"

static void		load_b(t_stacks *stacks, int len, int *tab, int dir)
{
	int		i;
	int		moved;
	t_list	**st;

	st = &(stacks->a);
	i = -1;
	moved = 0;
	while (++i < len && moved < len / 2)
	{
		if ((*(int *)((*st)->content) - tab[len / 2]) * dir < 0 && ++moved)
			push_st(stacks, FALSE);
		else
			rotate_st(stacks, TRUE);
	}
}

void			small_sort(t_stacks *stacks, int len, t_bool sort_a)
{
	if (!stack_is_sort(sort_a ? stacks->a : stacks->b, len, -1 + 2 * sort_a))
	{
		if (!compare(stacks, -1 + 2 * sort_a, sort_a))
		{
			if (len > 2 && !compare(stacks, -1 + 2 * !sort_a, !sort_a))
				swap_both(stacks);
			else
				swap_st(stacks, sort_a);
			if (len > 2)
				small_sort(stacks, len, sort_a);
		}
		else
		{
			push_st(stacks, !sort_a);
			small_sort(stacks, len - 1, sort_a);
			push_st(stacks, sort_a);
			small_sort(stacks, len, sort_a);
		}
	}
}

void			small_sort_rot(t_stacks *stacks,
					int len, t_bool sort_a, int dir)
{
	t_list		*st;

	st = sort_a ? stacks->a : stacks->b;
	if (!stack_is_sort(st, len, dir))
	{
		if (len < 3
			|| ((*(int *)(st->next->next->content)
			- *(int *)(st->next->content)) * dir > 0
			&& (*(int *)(st->next->next->content)
			- *(int *)(st->content)) * dir > 0))
		{
			if (!compare(stacks, -1 * dir, !sort_a))
				swap_both(stacks);
			else
				swap_st(stacks, sort_a);
		}
		else if (len > 2 && !compare(stacks, dir, sort_a))
			rotate_st(stacks, sort_a);
		else if (len > 2)
			revrotate_st(stacks, sort_a);
		small_sort_rot(stacks, len, sort_a, dir);
	}
}

void			sort_both(t_stacks *stacks, int len)
{
	if (len == 4)
	{
		if (!stack_is_sort(stacks->a, 2, 1))
		{
			if (!stack_is_sort(stacks->b, 2, -1))
				swap_both(stacks);
			else
				swap_st(stacks, TRUE);
		}
		else if (!stack_is_sort(stacks->b, 2, -1))
			swap_st(stacks, FALSE);
	}
	small_sort_rot(stacks, len / 2, FALSE, -1);
	small_sort_rot(stacks, len / 2 + len % 2, TRUE, 1);
}

void			simple_sort(t_stacks *stacks, int len)
{
	int		*tab;
	int		i;

	tab = list_to_tab(stacks->a, len);
	tab_quicksort(tab, len);
	if (len > 3)
	{
		load_b(stacks, len, tab, 1);
		sort_both(stacks, len);
		i = 0;
		while (i++ < len / 2)
			push_st(stacks, TRUE);
	}
	else
		small_sort_rot(stacks, len, TRUE, 1);
	free(tab);
}
