/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_st.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 18:51:28 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/02 17:59:17 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sort.h"
#include "stack_op.h"

void		swap_st(t_stacks *stacks, t_bool sort_a)
{
	stack_swap(sort_a ? &(stacks->a) : &(stacks->b), NULL);
	ft_lstaddend(&(stacks->op),
		ft_lstnew((void *)(sort_a ? "sa\n" : "sb\n"), sizeof(char) * 4));
}

void		swap_both(t_stacks *stacks)
{
	stack_swap(&(stacks->a), &(stacks->b));
	ft_lstaddend(&(stacks->op),
		ft_lstnew((void *)("ss\n"), sizeof(char) * 4));
}

void		push_st(t_stacks *stacks, t_bool dest_a)
{
	if (!dest_a ? stacks->a : stacks->b)
	{
		stack_push(dest_a ? &(stacks->a) : &(stacks->b),
			!dest_a ? &(stacks->a) : &(stacks->b));
		ft_lstaddend(&(stacks->op),
		ft_lstnew((void *)(dest_a ? "pa\n" : "pb\n"), sizeof(char) * 4));
	}
}

void		rotate_st(t_stacks *stacks, t_bool sort_a)
{
	stack_rotate(sort_a ? &(stacks->a) : &(stacks->b), NULL);
	ft_lstaddend(&(stacks->op),
		ft_lstnew((void *)(sort_a ? "ra\n" : "rb\n"), sizeof(char) * 4));
}

void		revrotate_st(t_stacks *stacks, t_bool sort_a)
{
	stack_revrotate(sort_a ? &(stacks->a) : &(stacks->b), NULL);
	ft_lstaddend(&(stacks->op),
		ft_lstnew((void *)(sort_a ? "rra\n" : "rrb\n"), sizeof(char) * 5));
}
