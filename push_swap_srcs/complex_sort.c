/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   complex_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 19:02:12 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/11 12:31:11 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "op_sort.h"
#include "sort.h"
#include "stacks.h"
#include "tab.h"
#include <stdlib.h>

#include "ft_printf.h"

static int		load_b(t_stacks *stacks, int len, int *tab, int dir)
{
	int		i;
	int		moved;
	int		saved;
	t_list	**st;

	st = &(stacks->a);
	i = -1;
	moved = 0;
	saved = 0;
	while (++i < len && (moved < len / 2
		|| *(int *)((*st)->content) == tab[len / 2]))
	{
		if (*(int *)((*st)->content) < tab[0]
			|| *(int *)((*st)->content) > tab[len - 1])
			break ;
		if (*(int *)((*st)->content) == tab[len / 2 + saved] && (++saved))
			rotate_st(stacks, TRUE);
		else if ((*(int *)((*st)->content) - tab[len / 2]) * dir < 0 && ++moved)
			push_st(stacks, FALSE);
		else
			rotate_st(stacks, TRUE);
	}
	return (moved);
}

static int		load_a(t_stacks *stacks, int len, int *tab, int *saved)
{
	int		i;
	int		moved;
	t_list	**st;

	st = &(stacks->b);
	i = -1;
	moved = 0;
	while (++i < len && moved < len / 2 + (len % 2))
	{
		if (*(int *)((*st)->content) == tab[*saved] && (++(*saved)))
		{
			push_st(stacks, TRUE);
			rotate_st(stacks, TRUE);
		}
		else if ((*(int *)((*st)->content) - tab[len / 2]) >= 0 && ++moved)
			push_st(stacks, TRUE);
		else
			rotate_st(stacks, FALSE);
	}
	return (moved);
}

static void		iter_b(t_stacks *stacks, int len, int *tab)
{
	int		moved;
	int		saved;

	saved = 0;
	if (len < 4)
	{
		small_sort_rot(stacks, len, FALSE, -1);
		while (stacks->b)
			push_st(stacks, TRUE);
	}
	else
	{
		moved = load_a(stacks, len, tab, &saved);
		iter_b(stacks, len - moved - saved, tab + saved);
		moved = load_b(stacks, len, tab, -1);
		iter_b(stacks, moved, tab + len - moved);
	}
}

static int		divide_a(t_stacks *stacks, int len, int *tab, int dir)
{
	int		i;
	int		moved;
	int		saved;
	t_list	**st;

	st = &(stacks->a);
	i = -1;
	moved = 0;
	saved = 0;
	while (++i < len && moved < len / 2 + len % 2)
	{
		if (dir == 1 && *(int *)((*st)->content) == tab[saved] && (++saved))
			rotate_st(stacks, TRUE);
		else if ((*(int *)((*st)->content) - tab[len / 2]) * dir < 0 && ++moved)
			push_st(stacks, FALSE);
		else if (dir == -1
			&& *(int *)((*st)->content) == tab[len / 2] && ++moved)
			push_st(stacks, FALSE);
		else
			rotate_st(stacks, TRUE);
	}
	return (moved);
}

void			complex_sort(t_stacks *stacks, int len)
{
	int		*tab;
	int		moved;

	tab = list_to_tab(stacks->a, len);
	tab_quicksort(tab, len);
	moved = divide_a(stacks, len, tab, 1);
	iter_b(stacks, moved, (tab + len / 2 - moved));
	moved = divide_a(stacks, len, tab, -1);
	goto_stpos(stacks, tab[0], TRUE);
	iter_b(stacks, moved, tab + len / 2);
	goto_stpos(stacks, tab[0], TRUE);
	free(tab);
}
