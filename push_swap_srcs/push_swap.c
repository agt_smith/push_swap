/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 15:34:45 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/09 14:21:24 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stacks.h"
#include "stack_op.h"
#include "sort.h"
#include "tab.h"
#include <unistd.h>
#include <stdlib.h>

int				main(int argc, char *argv[])
{
	t_stacks	stacks;
	int			len;

	get_stack(&(stacks.a), argc, argv, get_flags(argc, argv, &(stacks.env)));
	stacks.b = NULL;
	stacks.op = NULL;
	len = ft_lstlen(stacks.a);
	if (!stack_is_sort(stacks.a, -1, 1))
	{
		if (len < 7)
			simple_sort(&stacks, len);
		else
			complex_sort(&stacks, len);
	}
	clean_conflict(&(stacks.op));
	print_sorted_op(stacks.op, !(stacks.env.fd) ? 1 : stacks.env.fd);
	release_stack(&(stacks.a));
	release_stack(&(stacks.b));
	release_op(&(stacks.op));
	if (stacks.env.fd)
		close(stacks.env.fd);
	return (0);
}
