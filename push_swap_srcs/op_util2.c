/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_util2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 15:05:23 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/02 16:22:58 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op_sort.h"
#include <stdlib.h>

t_bool		compare(t_stacks *stacks, int dir, t_bool comp_a)
{
	t_list		*st;

	st = comp_a ? stacks->a : stacks->b;
	if (!st || !(st->next))
		return (TRUE);
	return ((*(int *)(st->content) - *(int *)(st->next->content)) * dir < 0);
}
