/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_arg.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 18:05:08 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/14 14:57:06 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stacks.h"
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>

static t_bool	is_doublon(int val, t_list *stack)
{
	if (!stack)
		return (FALSE);
	if (val == *(int *)(stack->content))
		return (TRUE);
	return (is_doublon(val, stack->next));
}

static t_bool	arg_is_valid(char *arg, int **val)
{
	int			i;
	int			sign;
	long		tmp;

	if (!val || !arg)
		ft_perror("error");
	i = 0;
	while (arg[i] && ft_isspace(arg[i]))
		i++;
	sign = (arg[i] == '-' || arg[i] == '+') ? 1 - 2 * (arg[i] == '-') : 1;
	i += (arg[i] == '-' || arg[i] == '+');
	tmp = 0;
	while (ft_isdigit(arg[i]))
	{
		tmp = (tmp * 10) + (arg[i] - '0');
		if ((sign < 0 && sign * tmp < INT_MIN) || (sign > 0 && tmp > INT_MAX))
			return (FALSE);
		i++;
	}
	if (arg[i])
		return (FALSE);
	if (!(*val = (int *)malloc(sizeof(int))))
		ft_perror("malloc error");
	**val = (int)(sign * tmp);
	return (TRUE);
}

int				get_flags(int argc, char *argv[], t_env *env)
{
	int		i;

	i = 1;
	env->flags = 0;
	env->fd = 0;
	while (i < argc && argv[i][0] == '-' && ft_isalpha(argv[i][1]))
	{
		if (argv[i][1] == 'v' && argv[i][2] == '\0')
			env->flags = env->flags | 1;
		else if (argv[i][1] == 'f' && argv[i][2] == '\0')
		{
			if ((env->fd = open(argv[++i], O_RDWR | O_CREAT, 0666)) < 0)
				ft_perror("error while opening file");
		}
		else
			ft_perror("error flag");
		i++;
	}
	return (i);
}

void			get_stack(t_list **stack, int argc, char *argv[], int start)
{
	int		i;
	int		j;
	int		*val;
	char	**split;

	if ((i = start) && !stack)
		ft_perror("[get_stack]Invalid arg");
	*stack = NULL;
	while (i < argc)
	{
		split = ft_strsplit(argv[i++], ' ');
		if (!split[0])
			ft_perror("error");
		j = -1;
		while (split[++j])
		{
			if (!arg_is_valid(split[j], &val) || is_doublon(*val, *stack))
				ft_perror("error");
			free(split[j]);
			ft_lstaddend(stack, ft_lstnew((void *)val, sizeof(int)));
			free(val);
		}
		free(split[j]);
		free(split);
	}
}
