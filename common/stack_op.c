/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_op.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 19:04:45 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/09 15:29:15 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			stack_swap(t_list **stack, t_list **bis)
{
	t_list		*tmp;

	if (!stack)
		ft_perror("[stack_swap]Invalid arg");
	if (*stack && (*stack)->next)
	{
		tmp = *stack;
		*stack = (*stack)->next;
		tmp->next = (*stack)->next;
		(*stack)->next = tmp;
		if (bis && *bis && (*bis)->next)
		{
			tmp = *bis;
			*bis = (*bis)->next;
			tmp->next = (*bis)->next;
			(*bis)->next = tmp;
		}
	}
}

void			stack_push(t_list **dst, t_list **src)
{
	t_list		*tmp;

	if (!dst || !src)
		ft_perror("[stack_push]Invalid arg");
	tmp = *src;
	if (*src)
		*src = (*src)->next;
	if (tmp)
	{
		tmp->next = (*dst);
		*dst = tmp;
	}
}

void			stack_rotate(t_list **stack, t_list **bis)
{
	t_list		*tmp;

	if (!stack)
		ft_perror("[stack_rotate]Invalid arg");
	if (*stack && (*stack)->next)
	{
		tmp = *stack;
		*stack = (*stack)->next;
		tmp->next = NULL;
		ft_lstaddend(stack, tmp);
		if (bis && *bis && (*bis)->next)
		{
			tmp = *bis;
			*bis = (*bis)->next;
			tmp->next = NULL;
			ft_lstaddend(bis, tmp);
		}
	}
}

void			stack_revrotate(t_list **stack, t_list **bis)
{
	t_list		**tail;

	if (!stack)
		ft_perror("[stack_revrotate]Invalid arg");
	if (*stack && (*stack)->next)
	{
		tail = ft_lsttail(stack);
		(*tail)->next = *stack;
		*stack = *tail;
		*tail = NULL;
		if (bis && *bis && (*bis)->next)
		{
			tail = ft_lsttail(bis);
			(*tail)->next = *bis;
			*bis = *tail;
			*tail = NULL;
		}
	}
}
