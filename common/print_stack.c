/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 13:51:03 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/10 14:31:06 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack_op.h"
#include "stacks.h"
#include "libft.h"
#include "ft_printf.h"
#include <unistd.h>

void			print_both(t_list *a, t_list *b, t_bool start, char *op)
{
	if (op)
		ft_printf("\033[93m%s\x1B[0m\n", op);
	if (start)
		ft_printf("\033[94m%20c\t%20c\x1B[0m\n", 'A', 'B');
	if (a)
		ft_printf("%20d", *(int *)(a->content));
	else if (b)
		ft_printf("%20s", "");
	if (b)
		ft_printf("\t%20d\n", *(int *)(b->content));
	else if (a)
		ft_printf("\n");
	if (a && b)
		print_both(a->next, b->next, FALSE, NULL);
	else if (a && !b)
		print_both(a->next, b, FALSE, NULL);
	else if (!a && b)
		print_both(a, b->next, FALSE, NULL);
}

static void		print_stackelem(t_list *elem)
{
	ft_putnbr(*(int *)(elem->content));
	ft_putchar('\n');
}

void			print_stack(t_list *stack)
{
	ft_lstiter(stack, &print_stackelem);
}

static void		print_opelem(t_list *elem)
{
	char		op[5];
	t_do_op		*op_elem;

	op_elem = (t_do_op *)(elem->content);
	ft_memset(op, (char)'\0', 5);
	op[0] = op_elem->id_op;
	if (ft_isupper(op[0]))
	{
		op[0] = ft_tolower(op[0]);
		op[1] = op[0];
	}
	op[1 + (op[1] != 0)] = op_elem->id_stack;
	op[2 + (op[2] != 0)] = '\n';
	write(1, op, ft_strlen(op));
}

void			print_op(t_list *lst_op)
{
	ft_lstiter(lst_op, &print_opelem);
}
