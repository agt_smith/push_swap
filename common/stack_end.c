/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_end.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 16:43:24 by gsmith            #+#    #+#             */
/*   Updated: 2017/12/18 16:04:16 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_bool			stack_is_sort(t_list *stack, int limit, int dir)
{
	if (!stack || !(stack->next) || limit == 1 || limit == 0)
		return (TRUE);
	if ((*(int *)(stack->content) - *(int *)((stack->next)->content)) * dir > 0)
		return (FALSE);
	return (stack_is_sort(stack->next, (limit < 0 ? -1 : limit - 1), dir));
}

int				stack_len_sort(t_list *stack, int limit, int dir)
{
	if (!stack || limit == 0)
		return (0);
	if (!(stack->next))
		return (1);
	if ((*(int *)(stack->content) - *(int *)((stack->next)->content)) * dir > 0)
		return (0);
	return (1 + stack_len_sort(stack->next, (limit < 0 ? -1 : limit - 1), dir));
}

static void		release_elem(void *val, size_t size)
{
	(void)size;
	free((int *)val);
}

void			release_stack(t_list **stack)
{
	ft_lstdel(stack, &release_elem);
}
