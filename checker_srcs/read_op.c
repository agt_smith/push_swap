/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_op.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 14:41:36 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/10 13:39:00 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack_op.h"
#include "stacks.h"
#include "get_next_line.h"
#include <stdlib.h>

static void		init_select_op(t_select_op select[NB_OP])
{
	if (!select)
		ft_perror("[init_select_op]Invalid arg");
	select[0].id = 's';
	select[0].op = &stack_swap;
	select[1].id = 'p';
	select[1].op = &stack_push;
	select[2].id = 'r';
	select[2].op = &stack_rotate;
	select[3].id = 'R';
	select[3].op = &stack_revrotate;
}

static void		valid_op(t_do_op op)
{
	if (!(op.id_stack == 'a' || op.id_stack == 'b'))
	{
		if (op.id_op == 'p')
			ft_perror("error");
		else if (op.id_op == 's' && op.id_stack != 's')
			ft_perror("error");
		else if ((op.id_op == 'r' || op.id_op == 'R') && op.id_stack != 'r')
			ft_perror("error");
	}
}

static t_do_op	read_op(char *str, t_list **a, t_list **b)
{
	static t_select_op	select[NB_OP] = {{'\0', NULL}};
	int					i;
	t_do_op				op;

	if (!(select[0].id))
		init_select_op(select);
	op.id_op = str[0];
	if (!(i = 0) && str[0] == 'r' && str[1] == 'r' && str[2])
		op.id_op = 'R';
	while (i <= NB_OP && select[i].id != op.id_op)
		if (i++ == NB_OP)
			ft_perror("error");
	op.op = select[i].op;
	op.id_stack = str[(op.id_op == 'R') + 1];
	if (str[(op.id_op == 'R') + 2])
		ft_perror("error");
	op.stack1 = (op.id_stack == 'b') ? b : a;
	if (op.id_op == 'p')
		op.stack2 = (op.id_stack == 'a') ? b : a;
	else
		op.stack2 = (op.id_stack == 'r' || op.id_stack == 's') ? b : NULL;
	valid_op(op);
	return (op);
}

void			get_op(t_list **lst_op, t_list **a, t_list **b, t_env env)
{
	char		*input;
	t_do_op		*op;
	int			witness;

	if (!lst_op)
		ft_perror("[get_op]Invalid arg");
	*lst_op = NULL;
	while ((witness = get_next_line(env.fd, &input)) > 0)
	{
		if (ft_strlen(input) > 3 || ft_strlen(input) < 2)
			ft_perror("error");
		if (!(op = (t_do_op *)malloc(sizeof(t_do_op))))
			ft_perror("malloc error");
		*op = read_op(input, a, b);
		ft_lstaddend(lst_op, ft_lstnew((void *)op, sizeof(t_do_op)));
		free(op);
		if (env.flags & 1)
		{
			exec_op(*ft_lsttail(lst_op));
			print_both(*a, *b, TRUE, env.fd != 0 ? input : NULL);
		}
		free(input);
	}
	if (witness < 0)
		ft_perror("[get_op]reading error");
}
