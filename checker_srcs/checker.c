/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 17:29:04 by gsmith            #+#    #+#             */
/*   Updated: 2018/01/10 13:20:35 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stacks.h"
#include "stack_op.h"
#include "read_op.h"
#include <unistd.h>
#include <stdlib.h>

int			main(int argc, char *argv[])
{
	t_list		*a;
	t_list		*b;
	t_list		*lst_op;
	t_env		env;

	get_stack(&a, argc, argv, get_flags(argc, argv, &env));
	if (ft_lstlen(a) < 1)
		return (0);
	b = NULL;
	if (env.flags & 1)
		print_both(a, b, TRUE, NULL);
	get_op(&lst_op, &a, &b, env);
	if (!(env.flags & 1))
		ft_lstiter(lst_op, &exec_op);
	release_op(&lst_op);
	if (stack_is_sort(a, -1, 1) && !b)
		ft_putendl("OK");
	else
		ft_putendl("KO");
	release_stack(&a);
	release_stack(&b);
	if (env.fd)
		close(env.fd);
	return (0);
}
