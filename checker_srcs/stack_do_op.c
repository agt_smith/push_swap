/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_do_op.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsmith <gsmith@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 20:36:21 by gsmith            #+#    #+#             */
/*   Updated: 2017/12/08 14:46:34 by gsmith           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack_op.h"
#include "stacks.h"
#include <stdlib.h>

void			exec_op(t_list *op)
{
	t_do_op		*do_op;

	do_op = (t_do_op *)(op->content);
	do_op->op(do_op->stack1, do_op->stack2);
}

static void		release_elem(void *val, size_t size)
{
	(void)size;
	free((t_do_op *)val);
}

void			release_op(t_list **lst_op)
{
	ft_lstdel(lst_op, &release_elem);
}
